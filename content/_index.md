---
title: "Startseite"
---

## Für ein freies und nachhaltiges Internet!

Unter *.graz.social werden einfach zu bedienende, quelloffene und **datenschutzfreundliche** Alternativen von Internetdiensten zur Verfügung gestellt. Es soll dabei, wie der Name verrät, einen Fokus auf Menschen in Graz und Umgebung geben.  [> Mehr erfahren <](/ueber-uns)

### Dienste

###### Mit folgenden Diensten kasst du zumindest schonmal Skype, Zoom und Twitter in wenigen Minuten ersetzen:
