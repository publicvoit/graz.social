---
title: Peertube
type: projekt
description: "Ein selbstverwaltetes Videoportal"
featured-image: peertube.jpg
link: "https://tube.graz.social"
---

Eine Videostreaming-Platform findet sich unter [https://tube.graz.social](https://tube.graz.social). Diese beeinhaltet nur originale Inhalte, direkt von den UrheberInnen.
[https://joinpeertube.org/](joinpeertube.org)

## Was ist Peertube
<div class="video-responsive">
<iframe width="100%" height="400" sandbox="allow-same-origin allow-scripts allow-popups" src="https://framatube.org/videos/embed/9c9de5e8-0a1e-484a-b099-e80766180a6d" frameborder="0" allowfullscreen></iframe>
</div>

## Hinweise
Da Videos sehr viel Speicherplatz benötigen ist eine Registrierung nur manuell möglich. Wenn du gerne einen Account hättest, zögere nicht und kontaktiere uns einfach.