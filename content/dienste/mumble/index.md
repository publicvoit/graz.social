---
title: Mumble
type: projekt
description: "Telefonkonferenz"
featured-image: mumble.jpg
---

Mumble ist ein schlankes und flinkes Open-Source-Programm für Gruppentelefonate. Benötigst du kein Video und es reicht dir zum teilen von Inhalten ein simpler Chat, bist du hier richtig.

### Benutzung
- Programme für alle Betriebsysteme findet man unter [mumble.info](https://www.mumble.info/)
- Für Android empfehlen wir die App [Mumla](https://f-droid.org/de/packages/se.lublin.mumla/)
- Die Adresse lautet **graz.social**, der Port ist standardmäßig **64738**

<img class="img-responsive" src="mumble.jpg"></img>

### Features
- Telefonkonferenzen mit vielen Teilnehmer:innen
- Hohe Sprachqualität
- Verschlüsselte Transportverbindung
- Anonyme Nutzung (Registrierung ist nicht erforderlich)
- Erstellen eigener (passwortgeschützter) Räume möglich
