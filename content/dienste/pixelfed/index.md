---
title: "Pixelfed"
type: "projekt"
subtitle: "Bilderbuch"
description: "Eine freie ethische Platfrom um deine Fotos zu teilen"
featured-image: pixelfed.jpg
link: "https://pixelfed.graz.social"
---

Wenn du im Internet fast ausschließlich Bilder teilen willst und dabei deine Sourveränität behalten willst,
ist eine Pixelfed-<span class="tooltip" data-tooltip="Wie bei E-Mail gibt es viele vernetzte Server">Instanz</span> die richtige Platform für dich.

### Features
- Bilder, Gallerien und Stories
- Jeder kann deinen Content sehen, auch ohne Registrierung
- folgen auch von z.B. [Mastodon](/dienste/mastodon) aus möglich (Teil des [Fediverse](https://de.wikipedia.org/wiki/Fediverse))

<img class="img-responsive" src="pixelfed.jpg"></img>

## Wichtige Hinweise
- Pixelfed befindet sich gerade noch in der Entwicklung
- Die Registrierung ist vorerst noch geschlossen