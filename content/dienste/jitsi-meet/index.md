---
title: "Jitsi-Meet"
type: "projekt"
subtitle: "Videokonferenz"
description: "Service für Telefon- oder Videokonferenzen in kleinen Gruppen im Browser."
featured-image: jitsi-meet.jpg
link: "https://meet.graz.social"
weight: 2
tags: ["Fediverse", "Soziales-Netzwerk"]
---

###### Audio- und Videokonferenz für kleinere Gruppen:
- direkt im Browser
- Anonyme Nutzung (Registrierung ist nicht erforderlich)
- Verschlüsselte Transportverbindung
- Screensharing möglich


<img class="img-responsive" src="jitsi-meet-systemlijpg"></img>

## Wichtige Hinweise
- Moderner Browser erforderlich: [Ungoogled-Chromium](https://ungoogled-software.github.io/ungoogled-chromium-binaries/), Chromium oder Chrome. 
- **Firefox und Safari machen noch Probleme**, leider für **alle**, auch wenn nur **ein einziger Teilnehmender** einen dieser Browser benutzt!
- Mobile Apps verfügbar, Android ([F-Droid ohne Google-Tracker](https://f-droid.org/de/packages/org.jitsi.meet/), [Play Store](https://play.google.com/store/apps/details?id=org.jitsi.meet&hl=en)) und [iOS](https://itunes.apple.com/us/app/jitsi-meet/id1165103905)
