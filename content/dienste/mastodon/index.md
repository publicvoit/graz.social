---
title: "Mastodon"
type: "projekt"
description: "Nachhaltiges und dezentrales soziales Netzwerk."
featured-image: mastodon.jpg
link: "https://graz.social"
weight: 1
---

Ein soziales Netzwerk in unseren Händen. Folge Freunden und entdecke neue, mit mehr als 4,4 Millionen Menschen. Veröffentliche alles was du willst: Links, Fotos, Text, Videos. Alles auf einer Plattform, die gemeinschaftsbasiert und werbefrei ist.

### Förderation - dezentral, aber Vernetzt

Genau wie beim Registrieren einer E-Mail-Adresse wird graz.social deinen Account hosten und Teil deiner Identität sein. **Und genauso kannst du allen folgen und mit jedem schreiben, egal von welchen Server aus!**

{{< youtube id="IPSbNdBmWKE" class="video-responsive">}}

## Hinweis

Mastodon ist nicht nur mit anderen Mastodon Instanzen vernetzt, sondern mit allen sozialen Netzwerken des **Fediverse**. Im Bild unten sieht man, dass man auch zum Beispiel auch einem **PeerTube**-Account genauso folgen kann.

<img class="img-responsive" src="mastodon.jpg"></img>