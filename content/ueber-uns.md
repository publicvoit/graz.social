---
title: "Über uns"
---

Alles was es bisher gibt ist aus einen spontanen Impus während eines Telefonates zwischen [André](https://graz.social/@linos) und [Thomas](https://graz.social/@tomgoe) innerhalb weniger Tage während der aktuellen Ausgangsbeschränkungen (April 2020) entstanden.

Zur Überlegung steht die Gründung eines Vereins namens "graz.social – Verein zur Förderung einer nachhaltigen digitalen Kultur in Graz".

Digitial-nachhaltig beudetet auch auf regionale Infrastruktur zu setzen. Momentan mieten wir einen Server mit Standort in Nürnberg (bei der [netcup GmbH](https://www.netcup.de/)) was vorwiegend auf die Spontanität dieses Projektes zurückgeht. Diese Lösung ist daher auf Dauer nicht optimal. 

Wir wurden schon öfters auf den Verein [mur.at](https://mur.at) (Verein zur Förderung von Netzwerkkunst) hingewiesen. Wir lieben dieses Projekt regelrecht und werden bald mal Kontakt aufnehmen, um zu sehen ob und inwieweit wir zusammenarbeiten können und dürfen.