---
title: "Dienste in Planung"
---
- [FramaDate](https://framadate.org/): Terminfinder – was doodle kann, kann es auch!
- [croodle](https://github.com/jelhan/croodle): Terminfinder, wie FramaDate, aber es tut sich mehr in der Entwicklung.
- [LimeSurvey](https://www.limesurvey.org/de): Erstellen von größeren Umfragen
- [EteSync](https://www.etesync.com/): Kalendersynchronisation: Ende-zu-Ende Verschlüsselt
- [Cryptpad](https://cryptpad.fr/): Gemeinsamer-Online-Dokumente-Editor
