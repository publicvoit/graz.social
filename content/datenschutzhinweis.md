---
title: "Hinweise zum Datenschutz für *.graz.social"
date: 2020-03-30
---


## Einführung

An dieser Stelle erwarten Sie bestimmt einen Satz wie:
```
Wir nehmen den Schutz Ihrer Daten sehr ernst.
```

Doch diese oder ähnliche Eingangsformulierungen werden Sie hier vergeblich suchen. Vielmehr habe ich das, was durch solche Sätze versucht wird auszudrücken, direkt in Taten umgesetzt.

So habe ich mich bspw. bewusst gegen den Einsatz von Tracking-Cookies, Analyse-Werkzeugen oder eine direkte Einbindung fremder Inhalte entschieden. Auch wenn mein Vorgehen nicht dem »Mainstream« entspricht und möglicherweise Nachteile für das Siteranking bei Google und Co. hat, habe ich mich bewusst für diesen Schritt entschieden. Dieses insbesondere auch deshalb, weil ich mir darüber im Klaren bin, dass ich mit jeder Einbindung von etwaigen (externen) Angeboten die Risiken für Ihr Recht auf informationelle Selbstbestimmung erhöhen bzw. Ihre IT-Sicherheit nur unnötig gefährden würde.

## 1. Name und Kontaktdaten des für die Verarbeitung Verantwortlichen

Diese Datenschutz-Information gilt für die Datenverarbeitung durch:
```
André Menrath BA BSc
Morellenfeldgasse 36
8010 Graz, Österreich
E-Mail: graz.social@posteo.at
```
Die Bestellung eines betrieblichen Datenschutzbeauftragten ist nicht erforderlich.

## 2. Erhebung und weitere Verarbeitung personenbezogener Daten inkl. Art und Zweck der Verwendung
### 2.1 Beim Besuch der Webseite

Wenn Sie meine Webseite besuchen, übermittelt Ihr Browser systembedingt gewisse Informationen an den Server, die Sie wiederum identifizierbar machen könnten. So übermitteln Sie bzw. Ihr Webbrowser z. B. Informationen wie die von Ihrem Provider zugewiesene IP-Adresse. Zu den Problematiken, die sich aus dieser systemimmanenten Datenübertragung ergeben können, finden Sie unter anderen Information auf den [Kuketz IT-Security Blog](https://kuketz-blog.de).

Dieses systembedingte Verhalten Ihres Browsers können Sie grundsätzlich nicht verhindern und es bleibt Ihnen deshalb auch nichts anderes übrig, als darauf zu vertrauen, dass ich mit den übermittelten Informationen sensibel umgehe. Weil mir – auch wenn es ein wenig abgedroschen klingen mag – der Schutz Ihrer Daten tatsächlich am Herzen liegt, werde ich die übermittelten Informationen bestmöglich Ihre Interessen vertretend handhaben. Aus diesem Grund erhebe, verarbeite oder nutze ich die übermittelten Informationen nicht zu Analyse-, Werbezwecken oder Ähnlichem. Vielmehr speichere ich keine Informationen über Sie oder Ihre Videochats – der Webserver auf Basis von nginx ist so konfiguriert, dass er keine Logfiles schreibt:

```
## LOGS ##
access_log off;
error_log off;
```

### 2.2 Videochats unter https://meet.graz.social

Zur Übertragung von Video- und Audiosignalen innerhalb der Videochats kommt die quelloffene Software Jitsi Meet zum Einsatz. Auf Basis von WebRTC werden Daten bzw. Media-Streams via Datagram Transport Layer Security (DTLS) und Secure Real-time Transport Protocol (SRTP) verschlüsselt übertragen. WebRTC bietet allerdings (noch) keine Möglichkeit, Videochats mit mehreren Personen Ende-zu-Ende zu verschlüsseln. Das bedeutet: Auf dem Transportweg bzw. im Netzwerk ist der Videochat verschlüsselt, auf dem Videochat-Server hingegen, der Jitsi Meet hostet, wird der gesamte Datenverkehr entschlüsselt und ist damit für den Betreiber einsehbar.

Als Betreiber speichere ich allerdings keine Informationen über Sie bzw. die Videochats. Es wird nichts protokolliert bzw. gespeichert / mitgeschnitten. Sofern Sie mir als Betreiber nicht vertrauen haben Sie ebenfalls die Möglichkeit, Ihre eigene Jitsi Meet Instanz zu hosten.

#### 2.2.1 Logfiles Videochat

Standardmäßig wird Jitsi Meet mit dem Logging-Level INFO ausgeliefert. In diesem Modus erfasst die Videobridge die IP-Adressen der Teilnehmer. Da ich diese Informationen nicht erfassen und speichern möchte, habe ich das Logging-Level auf WARNING gesetzt.

```
nano /etc/jitsi/videobridge/logging.properties
```
```
.level=WARNING
```

#### 2.2.2 STUN-Server

Das STUN-Protokoll erkennt Clients, die sich bspw. hinter einem Router oder einer Firewall befinden und eine NAT-Adresse haben. Mit Hilfe des STUN-Servers können NAT-Clients ihre öffentliche IP-Adresse erfahren und sind anschließend in der Lage eine direkte Kommunikationsverbindung zwischen (zwei) Teilnehmern herzustellen. Die Jitsi-Instanz nutzt folgende STUN-Server:

```
stun.nextcloud.com:443
stun.1und1.de:3478
stun.t-online.de:3478
```

### 2.3 Die Nutzung meiner Kontaktadresse

Bei Fragen jeglicher Art biete ich Ihnen die Möglichkeit, mit mir über die E-Mail-Adresse »andre.menrath@posteo.at« Kontakt aufzunehmen. Dabei ist die Angabe einer gültigen E-Mail-Adresse erforderlich, damit ich weiß, von wem die Anfrage stammt und um diese beantworten zu können. Weitere Angaben können freiwillig getätigt werden.

Die Datenverarbeitung zum Zwecke der Kontaktaufnahme mit mir erfolgt nach Art.6 Abs.1 S.1 lit.a DSGVO auf Grundlage Ihrer freiwillig erteilten Einwilligung.

## 3. Weitergabe von Daten

Eine Übermittlung Ihrer persönlichen Daten an Dritte zu anderen als den im Folgenden aufgeführten Zwecken findet nicht statt.

Ich gebe Ihre persönlichen Daten nur an Dritte weiter, wenn:

- Sie Ihre nach Art.6 Abs.1 S.1 lit.a DSGVO ausdrückliche Einwilligung dazu erteilt haben
- Die Weitergabe nach Art.6 Abs.1 S.1 lit.f DSGVO zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen erforderlich ist und kein Grund zur Annahme besteht, dass Sie ein überwiegendes schutzwürdiges Interesse an der Nichtweitergabe Ihrer Daten haben
- Für die Weitergabe nach Art.6 Abs.1 S.1 lit.c DSGVO eine gesetzliche Verpflichtung besteht
- Dies gesetzlich zulässig und nach Art.6 Abs.1 S.1 lit.b DSGVO für die Abwicklung von Vertragsverhältnissen mit Ihnen erforderlich ist

## 4. Betroffenenrechte

Sie haben das Recht:

- Gemäß Art.15 DSGVO Auskunft über Ihre von mir verarbeiteten personenbezogenen Daten zu verlangen. Insbesondere können Sie Auskunft über die Verarbeitungszwecke, die Kategorie der personenbezogenen Daten, die Kategorien von Empfängern, gegenüber denen Ihre Daten offengelegt wurden oder werden, die geplante Speicherdauer, das Bestehen eines Rechts auf Berichtigung, Löschung, Einschränkung der Verarbeitung oder Widerspruch, das Bestehen eines Beschwerderechts, die Herkunft ihrer Daten, sofern diese nicht bei mir erhoben wurden sowie über das Bestehen einer automatisierten Entscheidungsfindung einschließlich Profiling und ggf. aussagekräftigen Informationen zu deren Einzelheiten verlangen
- Gemäß Art.16 DSGVO unverzüglich die Berichtigung oder Vervollständigung Ihrer bei mir gespeicherten personenbezogenen Daten zu verlangen
- Gemäß Art.17 DSGVO die Löschung Ihrer bei mir gespeicherten personenbezogenen Daten zu verlangen, soweit nicht die Verarbeitung zur Ausübung des Rechts auf freie Meinungsäußerung und Information, zur Erfüllung einer rechtlichen Verpflichtung, aus Gründen des öffentlichen Interesses oder zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen erforderlich ist
- Gemäß Art.18 DSGVO die Einschränkung der Verarbeitung Ihrer personenbezogenen Daten zu verlangen, soweit die Richtigkeit der Daten von Ihnen bestritten wird, die Verarbeitung unrechtmäßig ist, Sie aber deren Löschung ablehnen, ich die Daten nicht mehr benötige, Sie jedoch diese zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen benötigen oder Sie gemäß Art.21 DSGVO Widerspruch gegen die Verarbeitung eingelegt haben
- Gemäß Art.20 DSGVO Ihre personenbezogenen Daten, die Sie mir bereitgestellt haben, in einem strukturierten, gängigen und maschinenlesbaren Format zu erhalten oder die Übermittlung an einen anderen Verantwortlichen zu verlangen
- Gemäß Art.7 Abs.3 DSGVO Ihre einmal erteilte Einwilligung jederzeit gegenüber mir zu widerrufen. Dies hat zur Folge, dass ich die Datenverarbeitung, die auf dieser Einwilligung beruhte, für die Zukunft nicht mehr fortführen darf
- Gemäß Art.77 DSGVO sich bei einer Aufsichtsbehörde zu beschweren. In der Regel können Sie sich hierfür an die Aufsichtsbehörde Ihres üblichen Aufenthaltsortes oder Arbeitsplatzes wenden

## 5. Widerspruchsrecht

Sofern Ihre personenbezogenen Daten auf Grundlage von berechtigten Interessen gemäß Art.6 Abs.1 S.1 lit.f DSGVO verarbeitet werden, haben Sie das Recht, gemäß Art.21 DSGVO Widerspruch gegen die Verarbeitung Ihrer personenbezogenen Daten einzulegen, soweit dafür Gründe vorliegen, die sich aus Ihrer besonderen Situation ergeben oder sich der Widerspruch gegen Direktwerbung richtet. Im letzteren Fall haben Sie ein generelles Widerspruchsrecht, das ohne Angabe einer besonderen Situation von mir umgesetzt wird.

Möchten Sie von Ihrem Widerrufs- oder Widerspruchsrecht Gebrauch machen, genügt eine E-Mail an »graz.social@posteo.at«.

## 6. Datensicherheit / Datenverarbeitung

Die gesamte Datenverarbeitung dieser Webseite erfolgt auf einem Server (Hosting durch netcup). Ich habe entsprechende technische und organisatorische Schutzmaßnahmen ergriffen, um potenziellen Angreifern ein Kompromittieren des Servers erheblich zu erschweren.

Beim Besuch dieser Webseite wird eine verschlüsselte Verbindung über das verbreitete TLS-Verfahren (Transport Layer Security) in Verbindung mit der jeweils höchsten Verschlüsselungsstufe (sog. Cipher-Suite) initiiert, die von Ihrem Browser unterstützt wird. Ob eine einzelne Seite meines Internetauftrittes verschlüsselt übertragen wird, erkennen Sie an der geschlossenen Darstellung des Schüssel- beziehungsweise Schloss-Symbols in der URL-Zeile Ihres Browsers.

Im Übrigen verwende ich geeignete technische und organisatorische Sicherheitsmaßnahmen, um Ihre Daten gegen zufällige oder vorsätzliche Manipulationen, teilweisen oder vollständigen Verlust, Zerstörung oder gegen den unbefugten Zugriff Dritter zu schützen. Meine getroffenen Sicherheitsmaßnahmen werden entsprechend der technologischen Entwicklung fortlaufend verbessert. Zu den Maßnahmen zählen unter anderem:

- Verschlüsselte Erreichbarkeit via HTTPS auf Basis moderner Cipher-Suites (TLS 1.2 mit 256-Bit-ECDSA-Zertifikat)

An folgenden Maßnahmen wird derzeit gearbeitet:
- Ausgefeilte Content-Security-Policy (CSP), bspw. zum Schutz gegen Cross-Site-Scripting
- Hardened GNU/Linux-Server auf Basis von Debian Buster
- Keine Einbindung von Ressourcen aus (unsicheren) Drittquellen
- Moderne Sicherheitsfeatures wie DNSSEC, OCSP-Stapling und CAA
- Einsatz aktueller HTTP-Security-Header wie HSTS, Expect-CT und Referrer-Policy
- […]

## 7. Aktualität und Änderung dieser Datenschutzerklärung

Diese Datenschutzerklärung ist aktuell gültig und hat den Stand April 2020.

Durch die Weiterentwicklung meiner Webseite und Angebote darüber oder aufgrund geänderter gesetzlicher beziehungsweise behördlicher Vorgaben kann es notwendig werden, diese Datenschutzerklärung zu ändern. Die jeweils aktuelle Datenschutzerklärung kann jederzeit auf der Webseite unter dem Link von Ihnen abgerufen und ausgedruckt werden.

## 8. Adressverarbeitung

Alle die auf dieser Webseite angegebenen Kontaktinformationen von mir inklusive etwaiger Fotos dienen ausdrücklich nur zu Informationszwecken bzw. zur Kontaktaufnahme. Sie dürfen insbesondere nicht für die Zusendung von Werbung, Spam und ähnliches genutzt werden. Einer werblichen Nutzung dieser Daten wird deshalb hiermit widersprochen. Sollten diese Informationen dennoch zu den vorstehend genannten Zwecken genutzt werden, behalte ich mir etwaige rechtliche Schritte vor.

## Hinweis
Diese Datenschutzerklärung wurde auf Basis der [Datenschutzerklärung von Kuketz IT-Security](https://www.kuketz-blog.de/datenschutzhinweis-kuketz-meet-de/) erstellt.