---
title: "Impressum"
date: 2020-03-30
---


### Postanschrift

```
André Menrath
Morellenfeldgasse 36  
8010 Graz
```

### E-Mail
graz.social@posteo.at
