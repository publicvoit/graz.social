---
title: "Unterstützen"
date: 2019-11-30
---

### Monatliche Kosten
- 19,00 €: für den Server
- 3,90 €: für die Domain graz.social

### Spenden
Unterstützen kann man uns wie folgt:
- https://liberapay.com/graz.social/
- PayPal: graz.social@posteo.at
- Andere Wege sind auch möglich, bitte dazu persönlich kontaktieren!

### Hinweis
Die Dienste werden freiwillig, kostenlos und ohne Gewinnabsicht bereitgestellt. Etwaige Überschüsse werden als Reserven angelegt. Falls das Spendenaufkommen die Kosten mehr als drei Monate in Folge nicht decken kann (unter Berücksichtigung von Reserven), wird eine Schließung der Dienste angekündigt, welche wiederum frühestens drei Monate später vollzogen wird.

### Transparenz
Die Höhe der eingelangten Spenden wird monatlich auf dieser Seite veröffentlicht.