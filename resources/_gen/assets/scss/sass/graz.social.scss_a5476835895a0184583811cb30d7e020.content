.masonry-wrapper {
  padding: 0.5rem 0;
  max-width: 960px;
  margin-right: auto;
  margin-left: auto; }

.masonry {
  columns: 1;
  column-gap: 1rem; }

.masonry-item {
  display: inline-block;
  vertical-align: top;
  margin-bottom: 1rem; }

@media only screen and (max-width: 1023px) and (min-width: 768px) {
  .masonry {
    columns: 2; } }

@media only screen and (min-width: 1024px) {
  .masonry {
    columns: 3; } }

.masonry-item, .masonry-content {
  border-radius: 4px;
  overflow: hidden; }

.masonry-item {
  filter: drop-shadow(0px 2px 2px rgba(0, 0, 0, 0.3));
  transition: filter .25s ease-in-out; }

.masonry-item:hover {
  filter: drop-shadow(0px 5px 5px rgba(0, 0, 0, 0.3)); }

.masonry-content {
  overflow: hidden; }

.masonry-item {
  color: #111111;
  background-color: #f9f9f9; }

.masonry-title, .masonry-description {
  margin: 0; }

.masonry-title {
  font-weight: 700;
  font-size: 1.1rem;
  padding: 1rem 1.5rem; }

.masonry-description {
  padding: 1.5rem;
  font-size: .75rem;
  border-top: 1px solid rgba(0, 0, 0, 0.05); }

.masonry-footer {
  font-size: .75em;
  opacity: .25;
  text-align: center;
  padding-top: 3em;
  padding-bottom: 3em;
  margin-bottom: -1.5em;
  transition: opacity 1s ease-in-out; }

.masonry-footer a {
  color: currentColor; }

.masonry-footer:hover, .masonry-footer:active, .masonry-footer:focus {
  opacity: .75; }

*, ::before, ::after {
  box-sizing: border-box; }

body {
  background-color: #fefefe;
  padding-bottom: 35px; }

h1, h2, h3, h4 {
  font-family: 'Gotu'; }

@media only screen and (min-width: 600px) {
  .main-wrapper {
    padding-right: 2.5rem; } }

@media only screen and (min-width: 600px) and (min-width: 800px) {
  .main-wrapper {
    padding-right: 3.5rem; } }

main {
  margin: 2rem 1rem 4rem 1rem; }
  @media only screen and (max-width: 600px) {
    main {
      margin: 1.2rem 0.5rem 3rem 0.5rem; } }
.sidebar {
  padding: 3rem 0;
  position: fixed;
  max-width: 426px !important;
  margin-left: 0.5rem;
  padding-left: 3.5rem; }
  @media only screen and (max-width: 800px) {
    .sidebar {
      padding-left: 2.5rem; } }
  @media only screen and (max-width: 600px) {
    .sidebar {
      position: relative;
      padding: 0 !important;
      margin-left: 0; } }
@media only screen and (max-height: 720px) and (min-width: 800px) {
  .sidebar {
    padding-top: 0.5rem; }
    .sidebar img {
      max-height: 75vh; } }

.site-title {
  position: fixed;
  margin: 2.4rem 0rem; }
  @media only screen and (max-width: 1000px) {
    .site-title {
      margin-top: 3.5vw; } }
  @media only screen and (max-width: 600px) {
    .site-title {
      position: absolute;
      margin-top: 0; } }
  .site-title h1 {
    font-family: 'Delius Swash Caps';
    font-size: 2rem;
    color: #4d4d4d; }
    @media only screen and (max-width: 1280px) {
      .site-title h1 {
        font-size: 3.4vw !important; } }
    @media only screen and (max-width: 600px) {
      .site-title h1 {
        margin: 1rem 0.7rem;
        font-size: 2.2rem !important; } }
.site-title:hover {
  text-decoration: none; }

.video-responsive {
  margin-bottom: 1.5rem; }

@media only screen and (max-width: 600px) {
  .is-not-homepage {
    display: none; } }

.card a, .card a:hover, .card a:visited {
  color: #5755d9;
  text-decoration: none; }

.card img {
  opacity: 0.95; }

.card:hover img {
  opacity: 1; }

.information-desktop {
  margin-top: 0.7rem; }
  .information-desktop button {
    margin: 4.5px 0; }
  .information-desktop a, .information-desktop a:hover, .information-desktop a:visited {
    text-decoration: none;
    color: inherit; }
  @media only screen and (max-width: 600px) {
    .information-desktop {
      display: none; } }
.information-mobile {
  display: none;
  margin: 0.7rem 2rem; }
  .information-mobile a, .information-mobile a:hover, .information-mobile a:visited {
    text-decoration: none;
    color: inherit; }
  @media only screen and (max-width: 600px) {
    .information-mobile {
      display: block; } }
.credit-link {
  color: #5755d9 !important; }

.inline-logo {
  vertical-align: text-bottom; }

.project-link {
  margin-bottom: 0.8rem; }

.project-title {
  margin-bottom: 0.2rem; }

.modal {
  z-index: 10 !important; }

pre {
  overflow-x: scroll !important; }

.altert {
  margin-top: 1.5rem; }

ul {
  margin-bottom: 1.5rem; }
